package com.docongbang.favorites.test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import java.awt.DisplayMode;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.PlatformUI;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.docongbang.favorites.views.FavoritesView;

/**
 * The class <code>FavoritesViewTest</code> contains tests for the class
 * {com.docongbang.favorites.views.FavoritesView}.
 * 
 * @author congbangdo
 *
 */
public class FavoritesViewTest {
  /**
   * The object that is being tested.
   * 
   * @see com.docongbang.favorites.views.FavoritesView
   */
  private FavoritesView testView;

  /**
   * Perform pre-test initialization.
   * 
   * @throws Exception
   */
  @Before
  public void setUp() throws Exception {
    // Initialize the test fixture for each test that is run.
    waitForJobs();
    testView = (FavoritesView) PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().showView(FavoritesView.ID);
    // Delay for 3 seconds so that the view can be seen
    waitForJobs();
    delay(3000);
    // Add additional setup code here.
  }

  /**
   * Run the view test.
   */
  @Test
  public void testView() {
    TableViewer viewer = testView.getViewer();
    Object[] expectedContent = new Object[] {"One", "Two", "Three"};
    Object[] expectedLabels = new String[] {"One", "Two", "Three"};

    // Assert valid content.
    IStructuredContentProvider contentProvider =
        (IStructuredContentProvider) viewer.getContentProvider();
    assertArrayEquals(expectedContent, contentProvider.getElements(viewer.getInput()));

    // Assert valid labels
    ITableLabelProvider labelProvider = (ITableLabelProvider) viewer.getLabelProvider();
    for (int i = 0; i < expectedLabels.length; i++) {
      assertEquals(expectedLabels[i], labelProvider.getColumnText(expectedContent[i], 1));
    }
  }

  /**
   * Perform post-test cleanup.
   * 
   * @throws Exception
   */
  @After
  public void tearDown() throws Exception {
    waitForJobs();
    PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage()
        .hideView((IViewPart) testView);
  }

  /**
   * Process UI input but do not return for the specified time interval.
   * 
   * @param waitTimeMillis The number of milliseconds.
   */
  private void delay(long waitTimeMillis) {
    Display display = Display.getCurrent();

    // If this is the UI thread, then process input
    if (display != null) {
      long endTimeMillis = System.currentTimeMillis() + waitTimeMillis;
      while (System.currentTimeMillis() < endTimeMillis) {
        if (!display.readAndDispatch()) {
          display.sleep();
        }
        display.update();
      }
    }
    // Otherwise, perform a simple sleep
    else {
      try {
        Thread.sleep(waitTimeMillis);
      } catch (InterruptedException e) {
        // Ignored.
      }
    }
  }

  /**
   * Wait until all background tasks are complete.
   */
  public void waitForJobs() {
    while (!Job.getJobManager().isIdle()) {
      delay(1000);
    }
  }
}
